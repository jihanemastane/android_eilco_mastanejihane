package com.example.retrofit

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

class crewAdapter(private val listcrew:List <Crew>): RecyclerView.Adapter<crewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): crewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_crew,parent,false)
        return crewHolder(view)    }

    override fun getItemCount() =  listcrew.size

    override fun onBindViewHolder(holder: crewHolder, position: Int) {
        val crew = listcrew[position]
        val crewPoster = "https://image.tmdb.org/t/p/w185"+crew.profile_path
        Picasso.get()
            .load(crewPoster)
            .into(holder.crewPhoto)
        holder.domain.text = crew.department
        holder.member.text = crew.name

    }
}