package com.example.retrofit

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.cast.*
import kotlinx.android.synthetic.main.crew.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class crewact: AppCompatActivity()  {
    override fun onCreate(savedInstanceState: Bundle?) {
        val BASE_URL = "https://api.themoviedb.org/3/movie/"
        val API_KEY = "e9cdd84c6faf9b7b12beb27d32b26c42"
        super.onCreate(savedInstanceState)
        setContentView(R.layout.crew)
        val queue = Volley.newRequestQueue(this)
        val ID = intent.getSerializableExtra("idFilm")
        val IDFilm =  ""+ID

        val builder = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
        val retrofit = builder.build()
        val myInterface = retrofit.create(Moviedetails::class.java)
        val call = myInterface.getCast(IDFilm, API_KEY)


        call.enqueue(object : Callback<castActors> {
            override fun onResponse(
                call: Call<castActors>, response:
                Response<castActors>
            ) {
                val results = response.body()

                print(results)
                val crew = results?.crew
                val myRecyclerView = findViewById<RecyclerView>(R.id.crewRecyclerView)
                val crewadapter = crewAdapter(crew!!)
                crewRecyclerView.adapter = crewadapter
                myRecyclerView.layoutManager = GridLayoutManager(this@crewact, 1)

                btn2crew.setOnClickListener{
                    val intent = Intent(this@crewact, Cast::class.java)
                    intent.putExtra("idFilm", IDFilm)
                    startActivity(intent)}
                btn3crew.setOnClickListener{
                    val intent = Intent(this@crewact, crewact::class.java)
                    intent.putExtra("idFilm", IDFilm)
                    startActivity(intent)}
                btn1crew.setOnClickListener{
                    val intent = Intent(this@crewact, detailFilm::class.java)
                    intent.putExtra("id", IDFilm)
                    startActivity(intent)}






            }

            override fun onFailure(call: Call<castActors>, t: Throwable) {
                Toast.makeText(this@crewact, "Error...!!!", Toast.LENGTH_SHORT).show()
            }
        })




    }
}
