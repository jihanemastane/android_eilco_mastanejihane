package com.example.retrofit
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

class retroAdapter (private val listrepo:List <Result>, private val onListClickListener: OnListClickListener) :
    RecyclerView.Adapter<retroViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): retroViewHolder {
    val view = LayoutInflater.from(parent.context).inflate(R.layout.item_api,parent,false)
        return retroViewHolder(view, onListClickListener)
    }

    override fun getItemCount() = listrepo.size

    override fun onBindViewHolder(holder: retroViewHolder, position: Int) {
        val film = listrepo[position]
        val filmPoster = "https://image.tmdb.org/t/p/w500"+film.poster_path
        Picasso.get()
                .load(filmPoster)
                .into(holder.filmposter)


    }




}