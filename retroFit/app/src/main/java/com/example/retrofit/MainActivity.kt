package com.example.retrofit

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class MainActivity : AppCompatActivity() , OnListClickListener {

    val BASE_URL = "https://api.themoviedb.org"
    val PAGE = 1
    val API_KEY = "e9cdd84c6faf9b7b12beb27d32b26c42"
    val CATEGORY = "popular"

    var filmsList = ArrayList<Result>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val builder = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
        val retrofit = builder.build()
        val myInterface = retrofit.create(GitHubClient::class.java)
        val call = myInterface.getMovies(CATEGORY, API_KEY, PAGE)


        call.enqueue(object : Callback<Repo> {
            override fun onResponse(
                call: Call<Repo>, response:
                Response<Repo>
            ) {
                val results = response.body()
                filmsList.addAll(results!!.results)
                val path = filmsList[1].poster_path
                print(path)
                val myRecyclerView = findViewById<RecyclerView>(R.id.myRecyclerView)
                val filmAdapter = retroAdapter(filmsList, this@MainActivity)
                myRecyclerView.adapter = filmAdapter
                myRecyclerView.layoutManager = GridLayoutManager(this@MainActivity, 3)


            }

            override fun onFailure(call: Call<Repo>, t: Throwable) {
                Toast.makeText(this@MainActivity, "Error...!!!", Toast.LENGTH_SHORT).show();
            }
        })



    }

        override fun onListClick(position: Int) {

            //Toast.makeText(this, "Id film : ${filmsList[position].id}", Toast.LENGTH_LONG).show()
            val intent = Intent(this,detailFilm::class.java)
            intent.putExtra("id", filmsList[position].id)
            startActivity(intent)

        }

    override fun onbtnClick() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}


