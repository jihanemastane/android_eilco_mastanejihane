package com.example.retrofit

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.toolbox.Volley
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.itemdetail.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Retrofit
import retrofit2.Response
import retrofit2.converter.gson.GsonConverterFactory


class detailFilm : AppCompatActivity(){
    val BASE_URL = "https://api.themoviedb.org/3/movie/"
    val API_KEY = "e9cdd84c6faf9b7b12beb27d32b26c42"
    var IDFilm = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.itemdetail)
        val queue = Volley.newRequestQueue(this)
        val ID = intent.getSerializableExtra("id")
         this.IDFilm =  ""+ID

        val builder = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
        val retrofit = builder.build()
        val myInterface = retrofit.create(Moviedetails::class.java)
        val call = myInterface.getMovies(IDFilm, API_KEY)


        call.enqueue(object : Callback<movie> {
            override fun onResponse(
                call: Call<movie>, response:
                Response<movie>
            ) {
                val results = response.body()
                val imageFilm = findViewById<ImageView>(R.id.filmPic)
                val titleFilm = findViewById<TextView>(R.id.movie_title)
                val overviewFilm = findViewById<TextView>(R.id.movie_overview)
                val releaseFilm = findViewById<TextView>(R.id.movie_release_date)
                print(results)
                val uriImage="https://image.tmdb.org/t/p/original"+results?.backdrop_path
                print(uriImage)
                Picasso.get()
                    .load(uriImage)
                    .into(imageFilm)

                titleFilm.text = results?.title
                overviewFilm.text = results?.overview
                releaseFilm.text = results?.release_date
                movie_runtime.text = results?.budget.toString()
                
                btn2.setOnClickListener{
                    val intent = Intent(this@detailFilm, Cast::class.java)
                    intent.putExtra("idFilm", IDFilm)
                    startActivity(intent)}
                btn3.setOnClickListener{
                    val intent = Intent(this@detailFilm, crewact::class.java)
                    intent.putExtra("idFilm", IDFilm)
                    startActivity(intent)}
                btn1.setOnClickListener{
                    val intent = Intent(this@detailFilm, detailFilm::class.java)
                    intent.putExtra("id", IDFilm)
                    startActivity(intent)}




            }

            override fun onFailure(call: Call<movie>, t: Throwable) {
                Toast.makeText(this@detailFilm, "Error...!!!", Toast.LENGTH_SHORT).show()
            }
        })




    }



}