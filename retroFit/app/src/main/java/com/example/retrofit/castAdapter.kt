package com.example.retrofit

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

class castAdapter(private val listCast:List <CastX>): RecyclerView.Adapter<castViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): castViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_cast,parent,false)
        return castViewHolder(view)    }

    override fun getItemCount() =  listCast.size

    override fun onBindViewHolder(holder: castViewHolder, position: Int) {
        val cast = listCast[position]
        val castPoster = "https://image.tmdb.org/t/p/w185"+cast.profile_path
        Picasso.get()
            .load(castPoster)
            .into(holder.castPhoto)

        holder.actor.text = cast.name.toString()
        holder.caracter.text = "as "+cast.character.toString()
    }
}