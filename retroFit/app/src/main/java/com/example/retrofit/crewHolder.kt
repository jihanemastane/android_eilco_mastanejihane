package com.example.retrofit

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_cast.view.*
import kotlinx.android.synthetic.main.item_crew.view.*

class crewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){
    val crewPhoto = itemView.crewPhoto
    var domain = itemView.domain
    var member = itemView.member
}