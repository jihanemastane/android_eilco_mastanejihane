package com.example.retrofit

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface Moviedetails {
    @GET("/3/movie/{movie_id}")
    fun getMovies(
        @Path("movie_id") movie_id: String,
        @Query("api_key") apiKey: String
    ): Call<movie>

    @GET("/3/movie/{movie_id}/credits?")
    fun getCast(
        @Path("movie_id") movie_id: String,
        @Query("api_key") apiKey: String
    ):Call<castActors>
}