package com.example.retrofit
import android.view.View
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_api.view.*

class retroViewHolder(itemView : View, private val onListClickListener: OnListClickListener) :RecyclerView.ViewHolder(itemView)  {

    init{
        itemView.setOnClickListener {
            onListClickListener.onListClick(adapterPosition)
        }
    }

    //val name: TextView = itemView.nameTxtView
    //val overView: TextView = itemView.overview
    val filmposter: ImageView = itemView.photo

    }




