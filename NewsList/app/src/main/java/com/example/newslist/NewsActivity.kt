package com.example.newslist

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_news.*

class NewsActivity : AppCompatActivity() {


    override fun onBackPressed() {
        super.onBackPressed()
            //Finir l'activité ici
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news)
        title = "NewsActivity"

        okBtn.setOnClickListener {
            val intent = Intent(this, DetailsActivity::class.java)
            startActivity(intent)

        }
        logoutBtn.setOnClickListener {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)

        }
    }
}
