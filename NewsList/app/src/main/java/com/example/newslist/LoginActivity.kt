package com.example.newslist

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.content.Intent

class LoginActivity : AppCompatActivity(){

    override fun onBackPressed() {
        super.onBackPressed()
            //Finir l'activité ici
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        title = "LoginActivity"
        val button: Button =  findViewById(R.id.button)
        button.setOnClickListener {
            val intent = Intent(this, NewsActivity::class.java)
            startActivity(intent)

        }

    }
}
