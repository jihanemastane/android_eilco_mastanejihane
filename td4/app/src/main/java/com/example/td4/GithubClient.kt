package com.example.td4

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface GithubClient {
    @GET("users/{username}/repos")
    fun UserRepositories(@Path("username") userName: String): Call<List<RepoList>>
}