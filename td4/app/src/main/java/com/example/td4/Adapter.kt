package com.example.td4

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class Adapter(private val list: List<RepoList>) :
    RecyclerView.Adapter<Holder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.gitrepo,parent,false)
        return Holder(view)
    }

    override fun getItemCount() = list.size


    override fun onBindViewHolder(holder: Holder, position: Int) {
        val item = list[position]
        holder.name.text = item.name
    }
}

