package com.example.td4

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val builder = Retrofit.Builder()
            .baseUrl("https://api.github.com")
            .addConverterFactory(GsonConverterFactory.create())

        val retrofit = builder.build()
        val client = retrofit.create(GithubClient::class.java)
        val call = client.UserRepositories("JakeWharton")
        call.enqueue(object : Callback<List<RepoList>> {
            override fun onResponse(call: Call<List<RepoList>>, response:
            Response<List<RepoList>>
            ) {
                val listRepoList = response.body()
                recyclerView.layoutManager =  LinearLayoutManager(this@MainActivity)
                recyclerView.adapter = Adapter(listRepoList!!)


            }
            override fun onFailure(call: Call<List<RepoList>>, t: Throwable) {
                Toast.makeText(this@MainActivity, "Error...!!!", Toast.LENGTH_SHORT).show();
            }
        })


    }
}
